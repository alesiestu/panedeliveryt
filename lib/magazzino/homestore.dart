import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:foodapp/track/ingressoa1.dart';
import 'package:foodapp/haccp/newcontrollo.dart';
import 'package:foodapp/haccp/day.dart';
import 'package:foodapp/haccp/questionario/controllo1.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/haccp/resolution.dart';

import 'package:foodapp/cronologia/prodotti/ingresso.dart';
import 'package:foodapp/cronologia/vendite/ingresso.dart';
import 'package:foodapp/cronologia/haccp/ingresso.dart';

import 'package:foodapp/cronologia/prodotti/homeprodotti.dart';

import 'package:foodapp/magazzino/home.dart';



class storeintro extends StatefulWidget{

 
 @override
 homestore createState() => new homestore(); 
}



class homestore extends State<storeintro>{
 DateTime _datacorrente= new DateTime.now();
 String _testodata = '';

 String controllostato='Non ancora effettuato';

 Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }
 
 
 
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
    lettura();
  }

  void lettura(){ 
  Firestore.instance
    .collection('controllogiornaliero')
    .where("data", isEqualTo: _testodata)
    .snapshots().listen((data) =>
        data.documents.forEach((doc) => 
        
        
         setState(() {
        controllostato=doc["data"];
          })
        

        
        
        ));
  
  
  }

  
 
 
 
  Widget build(BuildContext context){



    return new Scaffold(

      appBar: new AppBar(
        title: Text('Store'),
    
        actions: <Widget>[

          
        ],
      ),

      body: StaggeredGridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 12.0,
                mainAxisSpacing: 12.0,
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                children: <Widget>[
                  _buildTile(
                   
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                          (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              
                              Text('Materie prime in ingresso', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 20.0)),
                              

                            ],
                          ),
                          Material
                          (
                            
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(18.0),
                            child: Center
                            (
                              child: Padding
                              (
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.play_for_work, color: Colors.white, size: 20.0),
                                
                              )
                            )
                          )
                        ]
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new storeingresso()),
                    );

                    }
                  ),
                   _buildTile(
                   
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                          (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              
                              Text('Prodotti lavorati', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 20.0))
                            ],
                          ),
                          Material
                          (
                            
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(18.0),
                            child: Center
                            (
                              child: Padding
                              (
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.add_box, color: Colors.white, size: 20.0),
                                
                              )
                            )
                          )
                        ]
                      ),
                    ),
                    onTap: (){
                      Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new ingressocrono()),
                    );}
                  ),
                 

                  

                

                  
                 
                  
                  
                 
                ],
                staggeredTiles: [
                  StaggeredTile.extent(2, 110.0),
                  StaggeredTile.extent(2, 110.0),
                  StaggeredTile.extent(2, 110.0),
                  StaggeredTile.extent(2, 110.0),
                  
                ],
              )


    
    
    
    
    
    );
  }

   Widget _buildTile(Widget child, {Function() onTap}) {
            return Material(
              elevation: 14.0,
              borderRadius: BorderRadius.circular(12.0),
              shadowColor: Color(0x802196F3),
              child: InkWell
              (
                // Do onTap() if it isn't null, otherwise do print()
                onTap: onTap != null ? () => onTap() : () { print('Not set yet'); },
                child: child
              )
            );
          }






}