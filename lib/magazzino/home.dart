import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/cronologia/prodotti/dettagli.dart';


class storeingresso extends StatefulWidget{

  storeingresso({this.data});
  final String data;
  
  @override
  _storeingressoState createState() => new _storeingressoState(); 
}

class _storeingressoState extends State<storeingresso>{
 
   String data;

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data=widget.data;
  }
    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Magazzino'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("ingresso")//.where("dataproduzione", isEqualTo: data)
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatelessWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;
  void elimina(var document){
    Firestore.instance.runTransaction((transaction)async{
                                DocumentSnapshot snapshot=
                                await transaction.get(document);
                                await transaction.delete(snapshot.reference);
                              });
  }

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
                   
                   String urlimmagine=document[i].data['pathimmagine'].toString();
                   String etichetta= document[i].data['etichetta'].toString();
                   String nomefornitore= document[i].data['nomefornitore'].toString();
                   String ivafornitore= document[i].data['ivafornitore'].toString();
                   String quantita= document[i].data['quantita'].toString();
                   String note= document[i].data['note'].toString();
                   String data= document[i].data['data'].toString();
                   String pathimmagine=document[i].data['pathimmagine'].toString();
                   String lotto=document[i].data['lotto'].toString();
                   String scadenza=document[i].data['scadenza'].toString();

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
             title: 
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                      padding: const EdgeInsets.all(1.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(etichetta.toUpperCase(),style: new TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                                ),
                              ],
                            ),
                    ),
                    Column(children: <Widget>[
                          Image.network(urlimmagine,width: 200,height: 150),
                           Padding(
                              padding: const EdgeInsets.only(top:4.0),
                              child: Text('Data fornitura:'),
                            ),
                              Text(data),
                               Padding(
                              padding: const EdgeInsets.only(top:4.0),
                              child: Text('Fornitore:'),
                            ),
                              Text(nomefornitore),
                    ],),
                        ],
                      ),
                      




                subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                
               
                ])
                
                
              ),
              new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                     new FlatButton(
                              
                              child: Row( children: <Widget>[
                                Icon(Icons.delete,color: Colors.red,),
                                Text('Elimina',style: TextStyle(fontSize: 18),)
                              ], ),
                              
                              onPressed: () {

                                  showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Column(children: <Widget>[Text('Procedere?',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold  )), 
            
            
            
            
            
             ],),
            
            actions: <Widget>[
              
              FlatButton(
                child: Text('Annulla',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  Navigator.pop(context);
                  
                   
                 
                  
                  
                },
              ),
              FlatButton(
                child: Text('Disabilita',style: TextStyle(fontSize: 14)),
                onPressed:(){
         
                elimina(document[i].reference);
                Navigator.pop(context);
                
                }
                

                   
                 
                  
                  
                
              )
            ],
          )
        );
 
                              
                              
                              
                              },
                            ),
                  
                    
                  ],
                ),
              ),
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _storeingressoState createState() => new _storeingressoState();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
