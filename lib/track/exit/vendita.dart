import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:async';
import 'package:foodapp/track/lavorazione/test2.dart';
import 'dart:math';
import 'package:foodapp/track/stadi.dart';

import 'package:foodapp/track/lavorazione/util/lavorazionemodel.dart';
import 'package:foodapp/track/lavorazione/service/firestoredataservice.dart';
import 'package:flutter/cupertino.dart';
import 'package:foodapp/track/exit/chiusura.dart';


class lavorazione extends StatefulWidget{
  lavorazione({this.nomeprodotto,this.codicesessione});
  final String nomeprodotto;
  final int codicesessione;
  
  @override
  _lavorazione createState() => new _lavorazione();
}

class _lavorazione extends State<lavorazione>{
var codicesessione;
String nomeprodotto;
List<DocumentSnapshot> document1;
List<DocumentSnapshot> document2;

 _showDialoginizio() async {
    await Future.delayed(Duration(milliseconds: 50));
    showDialog(
        context: context,
        builder: (BuildContext context) {

         return AlertDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

          new FloatingActionButton(
       child: Icon(Icons.add_box),
       backgroundColor: Colors.green,
       onPressed:(){
        
      

       },
            
            
     )
          ],
        ),
        content:
            new Text('Seleziona i prodotti in uscita per il cliente $nomeprodotto ',style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
           
          
        actions: <Widget>[
          
          new FlatButton(
            onPressed: () {

               Navigator.pop(context);
            },
            child: new Text('OK'),
          ),
        ],
      );
          
         // return new Container(child: new Text('foo'));



        });
  }

@override
  void initState() {
    super.initState();
    nomeprodotto=widget.nomeprodotto;
    codicesessione=widget.codicesessione;
    _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
    _showDialoginizio();
  }

  
  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  var sessione;

  

  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }

  
  
  
  
  void chiudi(){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('venditafinita');
       await reference.add({
        "dataproduzione":_testodata,
        "codicevendita" : codicesessione, 
        "cliente": nomeprodotto,
        
       });
     });
  }

     Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Annullare inserimento?'),
        content: new Text('I dati non saranno salvati'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('NO'),
          ),
          new FlatButton(
            onPressed: () {

               Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => new stadi(),
                          ),
                          ModalRoute.withName('/'));
            },
            child: new Text('SI'),
          ),
        ],
      ),
    ) ?? false;
  }
  


@override
    Widget build(BuildContext context) {
   
    return WillPopScope(
      onWillPop: _onWillPop,
          child: Scaffold(
        appBar: new AppBar(
          title: Row(
            children: <Widget>[
              Text('$nomeprodotto')
            ],
          ),
          actions: <Widget>[
                
                IconButton(
                
                icon: Icon(Icons.check),
                onPressed: () {
                 
                               Navigator.of(context).push(new MaterialPageRoute(
                                  builder: (BuildContext context)=> new venditafinale(
                                   nomeprodotto: nomeprodotto, 
                                   codicesessione: codicesessione,
                                  )
                                )
                                );
              //   chiudi();
                 
                },
              ),
          ],
        ),
       
       body:   
       
       new Stack(
       children: <Widget>[ 
       StreamBuilder(
         stream: Firestore.instance
         .collection("prodottifiniti").where("abilitato", isEqualTo: "0")
         .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
          if(!snapshot.hasData)
          return new Container( child: Center(
            child: CircularProgressIndicator()
          ),
          );
         document1=snapshot.data.documents;
         return StreamBuilder(
         stream: Firestore.instance
         .collection("uscita").where("codicesessionevendita", isEqualTo: codicesessione)
         .snapshots(), 
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot1 ){
        if(!snapshot1.hasData)
        return new Container( child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('Download dati dal server ', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            CircularProgressIndicator(),
          ],
        )
          ),
          );
        List<DocumentSnapshot> document2=snapshot1.data.documents;
        return new ListaProdotti(document: document1,document2:document2,codicesessione:codicesessione,nomeprodotto: nomeprodotto,data: _testodata,);
        },   
       );
        //return new Nulla();
        }, 
       ),
       
       ]
      )
      ),
    );
    }
}



  
class ListaProdotti extends StatefulWidget{
  final String nomeprodotto;
  final codicesessione;
  ListaProdotti({this.document,this.nomeprodotto,this.document2,this.codicesessione,this.data});
  final List<DocumentSnapshot> document;
  final List<DocumentSnapshot> document2;
  final String data;
  @override
  _ListaProdottiState createState() => new _ListaProdottiState();  
}


class _ListaProdottiState extends State<ListaProdotti>{
  DateTime _datacorrente= new DateTime.now();
  String nomeprodotto;
  var codicesesione;
  String controllo="";
  String testodata;

  FirebaseFirestoreService db = new FirebaseFirestoreService();
  
  List<DocumentSnapshot> document;
  List<DocumentSnapshot> document2;
  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      document=widget.document;
      nomeprodotto=widget.nomeprodotto;
      document2=widget.document2;
      codicesesione=widget.codicesessione;
      testodata=widget.data;
    }
  
    reload(){
    Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new lavorazione(
                          
                           codicesessione: codicesesione,
                           nomeprodotto: nomeprodotto,
                          )
                        )
                        );
  }

  void delete(elemento){
    Firestore.instance.runTransaction((transaction)async{
            DocumentSnapshot snapshot=
            await transaction.get(elemento);
            await transaction.delete(snapshot.reference);
          }).whenComplete(
            conferma
     
          );
    
  }


  void adddatacheck(String nome,var codice, String data ){
      
       Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('uscita');
       await reference.add({
        
        "data" : testodata, 
        "dataproduzione": data,
        "nome":nome,
        "codiceproduzione":codice,
        
        "codicesessionevendita":codicesesione
       });
     });
     
         
  }

  void numero() {
  var rng = new Random();
  
  var next = rng.nextDouble() * 1000000;
  while (next < 100000) {
    next *= 10;
  }
  print(next.toInt());
  setState(() {
      var codicesessione=next.toInt();
    });
}
void _showDialog(elemento) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Cancellare selezione?"),
            ],
          ),
         
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("No"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Si"),
              onPressed: () {
                delete(elemento);
                
              },
            ),
          ],
        );
      },
    );
}

conferma() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Cancellazione effettuata"),
            ],
          ),
         
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new FlatButton(
                  child: new Text("TORNA ALLA LISTA"),
                  onPressed: () {
                   reload();
                  },
                ),
              ],
            ),
           
          ],
        );
      },
    );
}


  _showDialogexample(document) async{
    await showDialog<String>(
      context: context,
      builder: (BuildContext context){
        return new CupertinoAlertDialog(
          title: Padding(
            padding: const EdgeInsets.all(8.0),
            child: new Text('Seleziona i prodotti'),
          ),
          actions: <Widget>[
            new CupertinoDialogAction(
              isDestructiveAction: true,
              onPressed: (){Navigator.of(context).pop('Indietro');},
              child: new Text('Indietro'),
            ),
            new CupertinoDialogAction(
              isDestructiveAction: true,
              
              onPressed: (){
                
             Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new lavorazione(
                          
                           codicesessione: codicesesione,
                           nomeprodotto: nomeprodotto,
                          )
                        )
                        );
              
              
              
              },
              
              child: new Text('Conferma'),
            ),
          ],
          content: new SingleChildScrollView(
            child: new Material(
              child: new MyDialogContent(document: document, prodotto: nomeprodotto, sessione: codicesesione),
            ),
          ),
        );
      },
      barrierDismissible: false,
    );
  }

quantita() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Imposta la quantità"),
            ],
          ),
         
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new FlatButton(
                  child: new Text("AGGIUNGI"),
                  onPressed: () {
                   reload();
                  
                  
                  
                  
                  
                  },
                ),
              ],
            ),
           
          ],
        );
      },
    );
}


  
   @override
   Widget build(BuildContext context){


     return Scaffold(

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.add_box),
       backgroundColor: Colors.green,
       onPressed:(){
        _showDialogexample(document);
      

       },
            
            
     ),
     
   


 
            
            
     



  body:
   ListView.builder(
    
    padding: const EdgeInsets.all(4.0),
    
    itemCount: document2.length,
                      
    
    
    
    
     itemBuilder: (BuildContext context, int i) {
      
       
       String nome=document2[i].data['nome'].toString();
       String quantita=document2[i].data['quantita'].toString();
       String lotto=document2[i].data['codicelotto'].toString();
       String misura=document2[i].data['misura'].toString();
      
       
        
      return new Card (
        child:
             ListTile( 
              subtitle: Column(children: <Widget>[
                Text('Data/lotto: $lotto'),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Quantità : $quantita $misura'),
                ),
                
              ],),  
              title: 
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                padding: const EdgeInsets.all(1.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(nome.toUpperCase(),style: new TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                            
                          ),
                        ],
                      ),
              ),
              
                  ],
                ), 
            onLongPress: (){ _showDialog(document2[i].reference);

            },),
        );
     }
  ),
     );
  }
  _lavorazione createState() => new _lavorazione();
}



class MyDialogContent extends StatefulWidget {
 
 
  MyDialogContent({
   this.document,
   this.sessione,
   this.prodotto
  });
  var sessione;
  var prodotto;
  
  
  List<DocumentSnapshot> document;  //materie prime
  //final List<String> countries;

  @override
  _MyDialogContentState createState() => new _MyDialogContentState();
}

class _MyDialogContentState extends State<MyDialogContent> {
  int _selectedIndex = 0;

  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  var sessione;
  var prodotto;
  bool pressed = true;

  Map<int, bool> itemsSelectedValue = Map();


   List<String> _selectedTile = List();
    
   


  
  

    void adddatacheck(String nome,int codice, String data, String quantita, String lotto, String misura ){
      
       Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('uscita');
       await reference.add({
        
        "data" : _testodata, 
        "dataproduzione": data,
        "nome":nome,
        "codiceproduzione":codice,
        "quantita":quantita,
        "codicesessionevendita":sessione,
        "codicelotto":lotto,
        "misura":misura,

       });
     });
     
         
  }

 var isSelected = false;
 var mycolor=Colors.white;
 
 void toggleSelection() {
    
    setState(() {
      if (isSelected) {
        mycolor=Colors.white;
        isSelected = false;
      } else {
        mycolor=Colors.grey[300];
        isSelected = true;
      }
    });
  
  } 

   

  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }

     List selectProduct = List();
     void _onCategorySelected(bool selected, codice) {
    if (selected == true) {
      setState(() {
        selectProduct.add(codice);
       
        

      });
    } else {
      setState(() {
        selectProduct.remove(codice);
        
      });
    }
  }

  reload(){
    Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new lavorazione(
                          
                           codicesessione: sessione, 
                           nomeprodotto: prodotto,
                          )
                        )
                        );
  }


  void deletesingle(elemento){
    
    var ref_value;
    
    Firestore.instance
    .collection('lavorazione')
    .where('iddocumento', isEqualTo: elemento)
    .limit(1)
    .getDocuments()
    .then((doc) {
    ref_value = doc.documents[0].reference;
    });
    
    
    Firestore.instance.runTransaction((transaction)async{
            DocumentSnapshot snapshot=
            await transaction.get(ref_value);
            await transaction.delete(snapshot.reference);
    });
    
  }


  List<DocumentSnapshot> document;  
  Color color;
  bool _color;
  String quantita='';
  @override
  void initState(){
    super.initState();
     _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";

    document=widget.document;
    sessione=widget.sessione;
    prodotto=widget.prodotto;
    color = Colors.white;
     _color=true;
  }

  _getContent(){
    if (widget.document.length == 0){
      return new Container();
    }

    return new Column(
      children: <Widget>[

        Container(
                   height: 500.0, // Change as per your requirement
                   width: 500.0, // Change as per your requirement
                   child: new ListView.builder(
                   padding: const EdgeInsets.all(4.0),
                   itemCount: document.length,
                   itemBuilder: (BuildContext context, int i) {
                   String nome=document[i].data['prodotto'].toString();
                   int codice=document[i].data['codicesessione'];
                   String data=document[i].data['dataproduzione'].toString();
                   String lotto=document[i].data['codicelotto'].toString();
                   String misura=document[i].data['misura'].toString();

      return new Card (
        
        color: selectProduct.contains(document[i].documentID) ? Colors.blueAccent : Colors.white,
        child:
            
             ListTile(
              
             selected: selectProduct.contains(document[i].documentID),
             enabled:  !selectProduct.contains(document[i].documentID),
              title: 
              
                  Column(
                    children: <Widget>[
                      Column(children: <Widget>[
                      
              ],),
              
              
              Padding(
                  padding: const EdgeInsets.only(left:1.0),
                  child: Column(        
                          children: <Widget>[
                            
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(nome.toUpperCase(),style: new TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                            ),

                            Text('Data/lotto:'),
                            Text(lotto),

                            Padding(
                              padding: const EdgeInsets.only(top:4.0),
                              child: Text('Data produzione'),
                            ),
                            Text(data),
                          //  Text(controllo)
                          ],
                        ),
              ),
              
                    ],
                  ), 
           onLongPress: (){
             //implementare funzione dove settare non abilitato il documento
             //toggleSelection();
             // adddatacheck(etichetta,pathimmagine,nomefornitore,ivafornitore,quantita,note,data);
            
               showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Prodotto esaurito?"),
            ],
          ),
         
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new FlatButton(
                  child: new Text("No"),
                  onPressed: () {
                   Navigator.pop(context);
                  },
                ),
                new FlatButton(
                  child: new Text("Si"),
                  onPressed: () {
                   
                     Firestore.instance.runTransaction((Transaction transaction)async {
                    DocumentSnapshot snapshot=
                    await transaction.get(document[i].reference);
                    await transaction.update(snapshot.reference, {
                       "abilitato": '1',


                    });
                  });

                  showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    // return object of type Dialog
                    return AlertDialog(
                      title: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text("Rimozione effettuata"),
                        ],
                      ),
                    
                      actions: <Widget>[
                        // usually buttons at the bottom of the dialog
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new FlatButton(
                              child: new Text("TORNA ALLA LISTA"),
                        onPressed: () {
                        reload();
                                    },
                                  ),
                                ],
                              ),
                            
                            ],
                          );
                        },
                      );
                                
                              

                              },
                            ),
                          ],
                        ),
                      
                      ],
                    );
                  },
                );

             
           
           
           },
           onTap: (){

             //_onCategorySelected(true,document[i].documentID);
            // adddatacheck(etichetta,pathimmagine,nomefornitore,ivafornitore,quantita,note,data,document[i].documentID);
              //adddatacheck(nome,codice,data);

               showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Imposta la quantità"),
              new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              keyboardType: TextInputType.number,
              onChanged: (String str){
                setState(() {
                quantita=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.dashboard),
                hintText: "Quantità $misura",
                border: InputBorder.none

              ),
            ),
          ),


            ],
          ),
         
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new FlatButton(
                  child: new Text("AGGIUNGI"),
                  onPressed: () {
                   _onCategorySelected(true,document[i].documentID);
                   adddatacheck(nome,codice,data,quantita,lotto,misura);
                   Navigator.pop(context);
                  
                  
                  
                  },
                ),
              ],
            ),
           
          ],
        );
      },
    );



             //setState(() {
              // document[i].isSelected = !document[i].isSelected;
               //document[i].documentID.contains(test);
              // });
            
            // adddatacheck(etichetta,pathimmagine,nomefornitore,ivafornitore,quantita,note,data);
             
           }, ),
        );
       
       
       
       



       
      








     }




  ),
               ),

      
      
      
      
      
      
      
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return _getContent();
  }
}








