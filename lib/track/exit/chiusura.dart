import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/stadi.dart';


class venditafinale extends StatefulWidget{
  venditafinale({this.nomeprodotto,this.codicesessione,this.data});
  final String nomeprodotto;
  final int codicesessione;
  final String data;

  
  @override
  _chiusura createState() => new _chiusura();
}

class _chiusura extends State<venditafinale>{
  String nomeprodotto;
  String misura='';
  int _radioValue = 0;
  int codicesessione;
  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  String quantita='';
  String fattura='';
  String codicelotto;


    Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }
  void chiudi(){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('venditafinita');
       await reference.add({
        "dataproduzione":_testodata,
        "codicevendita" : codicesessione, 
        "cliente": nomeprodotto,
        "fattura":fattura,

       });
     });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nomeprodotto=widget.nomeprodotto;
    _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
    codicesessione=widget.codicesessione;
  }

    void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          misura = "pz";
          break;
        case 1:
          misura = "lt";
          break;
        case 2:
          misura = "kg";
          break;
      }
    });
  }

  @override
    Widget build(BuildContext context) {

      return Scaffold(
        appBar:AppBar(
          title: Text(
            '$nomeprodotto'.toUpperCase()
          ),
        ),


        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: 
          
          Column(
            children: <Widget>[
             Card(
              child: new Column(
                children: <Widget>[
                  new ListTile(
                    title: new Row(
                      children: <Widget>[
                        new Expanded( child: Text("Data vendita:", style:new TextStyle(fontSize:22.0,color: Colors.black ))),
                         new FlatButton(
                          onPressed: ()=> _selezionadata(context),
                           child:Text(_testodata, style:new TextStyle(fontSize:22.0,color: Colors.black )))
                      ],
                    ),
                  )
                ],
              ),

            ),

              Card(
                child:  Column(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                       Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: Text('Ddt/Fattura' ,style:new TextStyle(fontSize:18.0,color: Colors.black )),
                      ),
                       Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: TextField(
                         
                          
                          onChanged: (String str){
                            setState(() {
                            fattura=str;        
                                            });
                          },
                          decoration: new InputDecoration(
                           
                            hintText: "",
                           

                            ),
                          ),
                       ),
                     
                       
                    ],),
                    
                     
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        onPressed: (){
                          chiudi();
                          Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => new stadi(),
                          ),
                          ModalRoute.withName('/'));
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Salva')
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      
      );
  
  }

  _chiusura createState() => new _chiusura();
}

