import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:foodapp/track/ingressoa1.dart';
import 'package:foodapp/track/lavorazione/home.dart';
import 'package:foodapp/track/prodotti/home.dart';
import 'package:foodapp/track/exit/selectclient.dart';
import 'package:foodapp/track/lavorazione/selectproduct.dart';
import 'package:foodapp/track/lavorazione/esempio.dart';


class stadi extends StatelessWidget{




  Widget build(BuildContext context){



    return new Scaffold(

      appBar: new AppBar(
        title: Text('Gestione processi'),
    
        actions: <Widget>[

          
        ],
      ),

      body: StaggeredGridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 12.0,
                mainAxisSpacing: 12.0,
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                children: <Widget>[
                  _buildTile(
                   
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                          (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              
                              Text('Ingresso', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                            ],
                          ),
                          Material
                          (
                            
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(24.0),
                            child: Center
                            (
                              child: Padding
                              (
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.add_box, color: Colors.white, size: 30.0),
                                
                              )
                            )
                          )
                        ]
                      ),
                    ),
                    onTap: (){
                      Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new ingressoa1()),
                    );}
                  ),
                   _buildTile(
                   
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                          (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              
                              Text('Lavorazione', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                            ],
                          ),
                          Material
                          (
                            
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(24.0),
                            child: Center
                            (
                              child: Padding
                              (
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.play_for_work, color: Colors.white, size: 30.0),
                                
                              )
                            )
                          )
                        ]
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new selectproduct()),
                    );

                    }
                  ),

                

                  _buildTile(
                   
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                          (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              
                              Text('Uscita', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                            ],
                          ),
                          Material
                          (
                            
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(24.0),
                            child: Center
                            (
                              child: Padding
                              (
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.exit_to_app, color: Colors.white, size: 30.0),
                                
                              )
                            )
                          )
                        ]
                      ),
                    ),
                   onTap: () {
                     Navigator.push(
                     context,
                     MaterialPageRoute(
                    builder: (BuildContext context) => new selectclient()),
                    );
                    }
                  ),
                 
                 
                  
                  
                 
                ],
                staggeredTiles: [
                  StaggeredTile.extent(2, 110.0),
                  StaggeredTile.extent(2, 110.0),
                  StaggeredTile.extent(2, 110.0),
                  StaggeredTile.extent(2, 110.0),
                  
                ],
              )


    
    
    
    
    
    );
  }

   Widget _buildTile(Widget child, {Function() onTap}) {
            return Material(
              elevation: 14.0,
              borderRadius: BorderRadius.circular(12.0),
              shadowColor: Color(0x802196F3),
              child: InkWell
              (
                // Do onTap() if it isn't null, otherwise do print()
                onTap: onTap != null ? () => onTap() : () { print('Not set yet'); },
                child: child
              )
            );
          }






}