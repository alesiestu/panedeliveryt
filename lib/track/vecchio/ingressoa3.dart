import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/ingressofinale.dart';
import 'dart:io';

//modello classe
import 'package:foodapp/track/model/etichette.dart';




class ingressoa3 extends StatefulWidget{
  ingressoa3({this.nome, this.iva, this.index, this.urlimmagine, this.pathimmagine, this.data, this.filename });
  final String nome;
  final String iva;
  final String data;
  final String filename;
  final index;
  final String urlimmagine;
  final String pathimmagine;
  @override
  _ingressoa3State createState() => new _ingressoa3State(); 
}

class _ingressoa3State extends State<ingressoa3>{
  String nome;
  String iva;
  String pathimmagine;
  File image;
  String data;
  String nomeetichetta;
  String filename;
  String urlimmagine;




   void _addData(){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('etichette');
       await reference.add({
         "nome" : nomeetichetta,
       
       });
  
     });}

  
  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      nome=widget.nome;
      iva=widget.iva;
      pathimmagine=widget.pathimmagine;
      image=image;
      data=widget.data;
      filename=widget.filename;
      urlimmagine=widget.urlimmagine;
  
    }

    
 
    @override
    Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
         title: Text('Seleziona etichetta '),
          actions: <Widget>[

           ],
      ),

      floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.add),
       backgroundColor: Colors.blue,
       onPressed: () {

          showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Column(children: <Widget>[Text('Inserire una nuova etichetta'), 
            
            TextField(
                      
                      onChanged: (String str){
                        setState(() {
                          nomeetichetta=str; 
                                
                                          });
                        },
                        decoration: new InputDecoration(
                
                
                      
                

                              ),
                            ),
            
            
            
            
             ],),
            
            actions: <Widget>[
              
              FlatButton(
                child: Text('Annulla',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  Navigator.pop(context);
                  
                   
                 
                  
                  
                },
              ),
              FlatButton(
                child: Text('Inserisci',style: TextStyle(fontSize: 14)),
                onPressed:(){
                
                Firestore.instance.runTransaction((Transaction transsaction) async{
                  CollectionReference reference= Firestore.instance.collection('etichette');
                  await reference.add({
                    "nome" : nomeetichetta,
                    

                    
 
                  });
                  
                  
                });
                Navigator.pop(context);
                
                }
                

                   
                 
                  
                  
                
              )
            ],
          )
        );




       }
            
            
     ),
     floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
     bottomNavigationBar: new BottomAppBar(
       elevation: 20,
       color: Colors.grey,
       child: ButtonBar(
         children: <Widget>[],
       ),
     ),

     body:  new Stack(
     children: <Widget>[ 
    
     new Padding(
            padding: const EdgeInsets.only(top:8),
            child:
     StreamBuilder(
       stream: Firestore.instance
       .collection("etichette")
       .snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),
        );

      return new listaetichette(document: snapshot.data.documents,nome: nome,iva: iva, path: pathimmagine,data: data,filename: filename,);
      },
     ),
     ), 
     ]
    ));
    }
}









class listaetichette extends StatefulWidget {
  
  listaetichette({this.document, this.nome,this.iva,this.path,this.data, this.filename});
  
  final nome;
  final iva;
  final path;
  final data;
  final filename;
  final List<DocumentSnapshot> document;
 
  @override
  State<StatefulWidget> createState() => new cercaState();
}


class cercaState extends State<listaetichette> {

  String nome;
  String iva;
  String path;
  String data;
  String filename;
  List<DocumentSnapshot> document;

   @override
  void initState() {
    nome=widget.nome;
    iva=widget.iva;
    path=widget.path;
    data=widget.data;
    filename=widget.filename;
    document=widget.document;


    controller.addListener(() {
      setState(() {
        filter = controller.text;
      });
    });



    super.initState();
  }



   List<String> items = [
    "Apple",
    "Bananas",
    "Milk",
    "Water",
  ];


  
  TextEditingController controller = new TextEditingController();
  String filter;
  Icon actionIcon = new Icon(Icons.search);
  Widget appBarTitle = new Text("AppBar Title");

   @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }


  _search(){
    setState(() {
      if ( this.actionIcon.icon == Icons.search){
        this.actionIcon = new Icon(Icons.close);
        this.appBarTitle = new TextField(
          controller: controller,
          style: new TextStyle(color: Colors.white,),
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search,color: Colors.white),
              hintText: "Search...",
              hintStyle: new TextStyle(color: Colors.white)
          ),
        );}
      else {
        this.actionIcon = new Icon(Icons.search);
        this.appBarTitle = new Text("AppBar Title");
      }
    });
    controller.clear();
  }








  @override
  Widget build(BuildContext context){

    
      return Scaffold(
        

              body: ListView.builder(
          physics: const AlwaysScrollableScrollPhysics (),
            itemCount: document.length,
            
            itemBuilder: (BuildContext context, int i) {
            
              
            String nomeetichetta= document[i].data['nome'].toString();

               
    return new Padding(padding: new EdgeInsets.all(10.0),
          child: new Card(
            child: new Column(
              children: <Widget>[
                new ListTile(
                  title: new Text(
                      "Nome etichetta : "
                  ,style: TextStyle(fontSize: 15 ),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top:8.0),
                    child: new Text("$nomeetichetta ",style: TextStyle(fontSize: 20),),
                  ),
                  
                  
                ),
                new ButtonTheme.bar(
                  child: new ButtonBar(
                    children: <Widget>[
                      new FlatButton(
                        child: Row( children: <Widget>[
                          Icon(Icons.add_box),
                          Text('Seleziona',style: TextStyle(fontSize: 18),)
                        ], ),
                        onPressed: () { 

                          Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context)=> new ingressofinale(
                              nome: nome,
                              iva: iva,
                              pathimmagine: path,
                              etichetta: nomeetichetta,
                              data: data,
                              filename: filename,
                            )
                          )
                          );


                        },
                      ),
                      
                    ],
                  ),
                ),
              ],
            ),
          )
    );
  





              
            }),
      );

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _ingressoa3State createState() => new _ingressoa3State();
}
