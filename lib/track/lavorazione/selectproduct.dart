import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/lavorazione/lavorazione.dart';
import 'dart:math';







class selectproduct extends StatefulWidget{
 
  
  @override
  _selectproduct createState() => new _selectproduct();

}

class _selectproduct extends State<selectproduct>{

var codice;
String nomeprodotto;




  @override
    void initState() {
      // TODO: implement setState
    super.initState();
    numero();
    }

 void numero() {
  var rng = new Random();
  
  var next = rng.nextDouble() * 1000000;
  while (next < 100000) {
    next *= 10;
  }
  print(next.toInt());
  setState(() {
      codice=next.toInt();
    });
}
  

  @override
   Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Seleziona prodotto'),
        actions: <Widget>[
          
        ],
      ),
      floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.add),
       backgroundColor: Colors.blue,
       onPressed: () {

          showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Column(children: <Widget>[Text('Nuovo prodotto',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold  )), 
            
            TextField(
                      
                      onChanged: (String str){
                        setState(() {
                          nomeprodotto=str; 
                                
                                          });
                        },
                        decoration: new InputDecoration(
                
                
                      
                

                              ),
                            ),
            
            
            
            
             ],),
            
            actions: <Widget>[
              
              FlatButton(
                child: Text('Annulla',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  Navigator.pop(context);
                  
                   
                 
                  
                  
                },
              ),
              FlatButton(
                child: Text('Inserisci',style: TextStyle(fontSize: 14)),
                onPressed:(){
                
                Firestore.instance.runTransaction((Transaction transsaction) async{
                  CollectionReference reference= Firestore.instance.collection('prodotti');
                  await reference.add({
                    "nome" : nomeprodotto,
                    

                    
 
                  });
                  
                  
                });
                Navigator.pop(context);
                
                }
                

                   
                 
                  
                  
                
              )
            ],
          )
        );




       }
            
            
     ),
     floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
     bottomNavigationBar: new BottomAppBar(
       elevation: 20,
       color: Colors.grey,
       child: ButtonBar(
         children: <Widget>[],
       ),
     ),

      body:  new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("prodotti")
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaprodotti(document: snapshot.data.documents,codicesessione: codice,);
      },

      
      
     ),
     ),
      
     ]
    ),

 
    );
    
    
  
  }



}


class listaprodotti extends StatelessWidget {
  
  listaprodotti({this.document, this.nome,this.iva,this.path,this.codicesessione});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;
  final int codicesessione;

  void elimina(var document){
    Firestore.instance.runTransaction((transaction)async{
                                DocumentSnapshot snapshot=
                                await transaction.get(document);
                                await transaction.delete(snapshot.reference);
                              });
  }
  

  @override
  Widget build(BuildContext context){

    
    
          
         return ListView.builder(
              itemCount: document.length,
              
              itemBuilder: (BuildContext context, int i) {
              
                
             
                String nome= document[i].data['nome'].toString();
              

                 
    return new Padding(padding: new EdgeInsets.all(2.0),
            child: Column(
              children: <Widget>[
               
                new Card(
                  child: new Column(
                    children: <Widget>[
                      
                      new ListTile(
                        title: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                               nome.toUpperCase()
                            ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold  ),
                            ),
                          ],
                        ),
                        
                        
                      ),
                      new ButtonTheme.bar(
                        child: new ButtonBar(
                          
                          children: <Widget>[
                             new FlatButton(
                              
                              child: Row( children: <Widget>[
                                Icon(Icons.delete,color: Colors.red,),
                                Text('Disabilita',style: TextStyle(fontSize: 18),)
                              ], ),
                              
                              onPressed: () {

                                  showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Column(children: <Widget>[Text('Procedere?',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold  )), 
            
            
            
            
            
             ],),
            
            actions: <Widget>[
              
              FlatButton(
                child: Text('Annulla',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  Navigator.pop(context);
                  
                   
                 
                  
                  
                },
              ),
              FlatButton(
                child: Text('Disabilita',style: TextStyle(fontSize: 14)),
                onPressed:(){
         
                elimina(document[i].reference);
                Navigator.pop(context);
                
                }
                

                   
                 
                  
                  
                
              )
            ],
          )
        );
 
                              
                              
                              
                              },
                            ),
                            new FlatButton(
                              
                              child: Row( children: <Widget>[
                                Icon(Icons.add_box),
                                Text('Seleziona',style: TextStyle(fontSize: 18),)
                              ], ),
                              
                              onPressed: () { 

                               Navigator.of(context).push(new MaterialPageRoute(
                                  builder: (BuildContext context)=> new lavorazione(
                                   nomeprodotto: nome, 
                                   codicesessione: codicesessione,
                                  )
                                )
                                );


                              },
                            ),
                            
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
    );
  





                
              });
     

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _selectproduct createState() => new _selectproduct();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}

