import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:foodapp/track/lavorazione/lavorazione.dart';
import 'package:foodapp/track/lavorazione/dettagli.dart';
import 'package:foodapp/track/lavorazione/util/query.dart';



class homestorico extends StatefulWidget{
  homestorico({this.data,this.fornitore});
  final String data;
  final String fornitore;
  
  @override
  _home createState() => new _home();

}


class _home extends State<homestorico>{


 List<String> selectedCities = [];
 DateTime _datacorrente= new DateTime.now();
 String _testodata = '';
 String fornitorestorico;
 

 Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        fornitorestorico=widget.fornitore;
        
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }

var lavori;

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    _testodata=widget.data;
    
    



  }
  
  
   

  


@override
    Widget build(BuildContext context) {
   
    return Scaffold(
      


      appBar: new AppBar(
        title: Text('Prodotti in lavorazione'),
    
        actions: <Widget>[


          
        ],
      ),
      

       
      floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.save),
       backgroundColor: Colors.blue,
       onPressed:(){
        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new lavorazione(
                            
                          
                          )
                        ));
        
      

       },
            
            
             ),
            floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
            bottomNavigationBar: new BottomAppBar(
              elevation: 20,
              color: Colors.grey,
              child: ButtonBar(
                children: <Widget>[],
              ),
            ),

    body: 
    
    new Stack(
     
     children: 
     
     <Widget>[ 
     
     Padding(
       padding: const EdgeInsets.all(8.0),
       child: ListTile(
              title: 
              Column(
              children: <Widget>[
              Text('Lavorazioni in data $_testodata', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
              
              
         ],),
         
       ),

     ),
     
     
     new Padding(
            padding: const EdgeInsets.only(top:80),
            child:
     
     StreamBuilder(

       stream: Firestore.instance
       .collection("lavorazione").where("datalavorazione", isEqualTo: _testodata).where("nomefornitore",isEqualTo: fornitorestorico )
       .snapshots(),

      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
           return new Container( child: Center(
           child: CircularProgressIndicator()
        ),

        );
      
      
      
      
      
      
      return new dettaglilavorazioni(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )
          



    );
    }


}

class dettaglilavorazioni extends StatefulWidget{

    final List<DocumentSnapshot> document;
    dettaglilavorazioni({this.document});
    @override
    _ListalavoriState createState() => new _ListalavoriState();
  
}


class _ListalavoriState extends State<dettaglilavorazioni>{
 
 var oggetto={'primo':'secondo','secondo':'test'};

 
 List<DocumentSnapshot> document;

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      document=widget.document;
      print(oggetto['primo']);
      



      


      



      


    }


 

 @override
   Widget build(BuildContext context){
     String etichetta='';
     String datalavorazione='';

     return new Scaffold(
       //inizio scaffold


       body:   
       
       
       
       ListView.builder(
            itemCount: document.length,
          
            itemBuilder: (BuildContext context, int i) {
            String pathimmagine=document[i].data['pathimmagine'];
            String etichetta=document[i].data['etichetta'];
            String fornitore=document[i].data['nomefornitore'];
          
          
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: Row(
                  children: <Widget>[
                   Column(
                      children: <Widget>[
                         Image.network(pathimmagine,width: 100,height: 130),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:8.0),
                      child: Column(children: <Widget>[
                      
                        Text('Etichetta', style: TextStyle(fontSize: 18),),
                        Text(etichetta,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),
                        
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child:
                          Column(
                            children: <Widget>[
                            Text('Fornitore', style: TextStyle(fontSize: 18),),
                            Text(fornitore,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),

                            ],
                          ) 
                         

                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child:
                          Column(
                            children: <Widget>[
                            Text('Data lavorazione', style: TextStyle(fontSize: 18),),
                            Text(fornitore,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),

                            ],
                          ) 
                         

                        ),
                        
                        
                      ],),
                    )
                  ],
                ),
              ),
               new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                      child: Row( children: <Widget>[
                        Icon(Icons.remove_red_eye),
                        Text('Dettagli',style: TextStyle(fontSize: 18),)
                      ], ),  
                      onPressed: () { 
                        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new dettagli(
                           path: pathimmagine, 
                          )
                        )
                        );
                      },
                    ),
                  ],
                ),
              ),
              
            ],
          ),
        )
    ); 
          }),

       //finescaffol

     );
 
   }   _home createState() => new _home();
}








