import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:foodapp/track/prodotti/storico.dart';
import 'package:foodapp/track/prodotti/newproduct.dart';



class cronoprodotti extends StatefulWidget{
  cronoprodotti({this.data});
  final String data;
  
  @override
  _home createState() => new _home();

}


class _home extends State<cronoprodotti>{
 String data;
 List<String> selectedCities = [];
 DateTime _datacorrente= new DateTime.now();
 


 

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    data=widget.data;
  }
@override
    Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text('Prodotti'),
        actions: <Widget>[
      
            ],
      ),  
      floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.save),
       backgroundColor: Colors.blue,
       onPressed:(){
        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new newproduct( 
                          )
                        ));
        
      

       },
            
            
             ),
            floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
            bottomNavigationBar: new BottomAppBar(
              elevation: 20,
              color: Colors.grey,
              child: ButtonBar(
                children: <Widget>[],
              ),
            ),

    body: 
    
    new Stack(
     
     children: 
     
     <Widget>[ 
     
     Padding(
       padding: const EdgeInsets.all(8.0),
       child: ListTile(
              title: 
            
              
                    
                      new Row(
                          children: <Widget>[
                            Text('Cronologia prodotti del $data')
                             
                          ],
                        ),
                   
                 
               
              
             
        
        
       ),

     ),
     
     
     new Padding(
            padding: const EdgeInsets.only(top:80),
            child:
     
     StreamBuilder(

       stream: Firestore.instance
       .collection("prodotti").where("dataproduzione", isEqualTo: data)
       .snapshots(),

      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
           return new Container( child: Center(
           child: CircularProgressIndicator()
        ),

        );
      
      
      
      
      
      
      return new dettaglilavorazioni(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )
          



    );
    }


}

class dettaglilavorazioni extends StatefulWidget{

    final List<DocumentSnapshot> document;
    dettaglilavorazioni({this.document});
    @override
    _ListalavoriState createState() => new _ListalavoriState();
  
}


class _ListalavoriState extends State<dettaglilavorazioni>{
 

 
 List<DocumentSnapshot> document;

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      document=widget.document;
      
      



      


      



      


    }


 

 @override
   Widget build(BuildContext context){
     String etichetta='';
     String datalavorazione='';

     return new Scaffold(
       //inizio scaffold


       body:   
       
       
       
       ListView.builder(
            itemCount: document.length,
          
            itemBuilder: (BuildContext context, int i) {
            String nome=document[i].data['nome'].toString();
            String quantita=document[i].data['quantita'].toString();   
            String dataproduzione=document[i].data['dataproduzione'].toString();
          
          
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: Row(
                  children: <Widget>[
                   
                    Padding(
                      padding: const EdgeInsets.only(left:8.0),
                      child: Column(
                       
                        children: <Widget>[

                        Row(children: <Widget>[
                        Text('Nome:', style: TextStyle(fontSize: 18),),
                        Text(nome,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),

                        ],),
                      
                        
                        
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child:
                          Row(
                            children: <Widget>[
                            Text('Quantità:', style: TextStyle(fontSize: 18),),
                            Text(quantita,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),

                            ],
                          ) 
                         

                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child:
                          Column(
                            children: <Widget>[
                            Text('Data produzione', style: TextStyle(fontSize: 18),),
                            Text(dataproduzione,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),

                            ],
                          ) 
                         

                        ),
                        
                        
                      ],),
                    )
                  ],
                ),
              ),
               new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                      child: Row( children: <Widget>[
                        Icon(Icons.remove_red_eye),
                        Text('Elimina',style: TextStyle(fontSize: 18),)
                      ], ),  
                      onPressed: () { 
                       
                      },
                    ),
                  ],
                ),
              ),
              
            ],
          ),
        )
    ); 
          }),

       //finescaffol

     );
 
   }   _home createState() => new _home();
}








