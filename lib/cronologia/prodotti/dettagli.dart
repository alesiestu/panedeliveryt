import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/ingressoa2.dart';


class dettagliprodotti extends StatefulWidget{

  dettagliprodotti({this.sessione});
  final sessione;
  
  @override
  _dettagliprodottiState createState() => new _dettagliprodottiState(); 
}

class _dettagliprodottiState extends State<dettagliprodotti>{
 
   var sessione;

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sessione=widget.sessione;
  }
    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Dettagli'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("lavorazione").where("codicesessione", isEqualTo: sessione)
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatelessWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String etichetta= document[i].data['materieprime'].toString();
            String dataingresso= document[i].data['dataingresso'].toString();
            String fornitore= document[i].data['nomefornitore'].toString();
            String note= document[i].data['note'].toString();
            String quantita= document[i].data['quantita'].toString();
            String immagine= document[i].data['pathimmagine'].toString();
            String lotto= document[i].data['lotto'].toString();
            String scadenza= document[i].data['scadenza'].toString();


            

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: new Text(
                   etichetta.toUpperCase()
                ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold ),textAlign: TextAlign.center,
                ),
                subtitle: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: Image.network(immagine,width: 150,height: 150),
                   ),
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child:
                     Column(
                       mainAxisAlignment: MainAxisAlignment.start,
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         Padding(
                           padding: const EdgeInsets.only(top:8.0),
                           child: Text('Scheda ingresso'.toUpperCase(), style: new TextStyle(fontWeight: FontWeight.bold),),
                         ),
                         Text('Fornitura del $dataingresso '),
                         Text('Fornitore: $fornitore'),
                         Text('Eventuali note : $note'),
                         Text('Quantità: $quantita'),
                         Text('Lotto: $lotto'),
                         Text('Data di scadenza: $scadenza'),
                       ],
                     )
                     
                   ),
                
               
                ])
                
                
              ),
              
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _dettagliprodottiState createState() => new _dettagliprodottiState();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
