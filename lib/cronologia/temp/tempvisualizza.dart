import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:foodapp/track/ingressoa3.dart';
import 'package:flutter/services.dart';
import 'package:foodapp/cronologia/vendite/homevendite.dart';
import 'package:foodapp/haccp/temperaturefrigo2.dart';
import 'package:foodapp/haccp/newfrigo.dart';
import 'package:foodapp/haccp/editfrigo.dart';



class tempcrono extends StatefulWidget{
  
 tempcrono({this.data});
  var data;

  @override
  _tempcronoState createState() => new _tempcronoState(); 
}




class _tempcronoState extends State<tempcrono>{

  
  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  String controllostato;


  String frigo1Am;
  String frigo1Pm;

  String frigo2Am;
  String frigo2Pm;

  String frigo3Am;
  String frigo3Pm;

  String frigo4Am;
  String frigo4Pm;

  String frigo5Am;
  String frigo5Pm;

  var data;


  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );
   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        
          });
   }
  }

  void lettura(){ 
  Firestore.instance
    .collection('temp')
    .where("data", isEqualTo: _testodata)
    .snapshots().listen((data) =>
        data.documents.forEach((doc) => 
        
        
         setState(() {
        controllostato=doc["data"];

          frigo1Am=doc["frigo1am"];
          frigo1Pm=doc["frigo1pm"];

          frigo2Am=doc["frigo2am"];
          frigo2Pm=doc["frigo2pm"];

          frigo3Am=doc["frigo3am"];
          frigo3Pm=doc["frigo3pm"];

          frigo4Am=doc["frigo4am"];
          frigo4Pm=doc["frigo4pm"];

          frigo5Am=doc["frigo5am"];
          frigo5Pm=doc["frigo5pm"];





          })
        

        
        
        ));
  
  
  }

  @override
    void initState() {
   
      super.initState();
    data=widget.data;
     lettura();
      _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);
    }
  @override
    Widget build(BuildContext context) {
        
   return new Scaffold(
    appBar: 
      new AppBar(
        title: Text('Scheda frigoriferi'),
      ),

      floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.add_box),
       backgroundColor: Colors.blue,
       onPressed: (){
          Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new newfrigo(
                        
                      )),
                    );

       }
              
     ),
     floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
     bottomNavigationBar: new BottomAppBar(
       elevation: 20,
       color: Colors.grey,
       child: ButtonBar(
         children: <Widget>[],
       ),
     ),
  
    body: 
      new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("temp").where("data", isEqualTo: data)
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )

    );
    }
}


class listaclienti extends StatelessWidget {
  
  listaclienti({this.document});
  final List<DocumentSnapshot> document;

      void elimina(var document){
    Firestore.instance.runTransaction((transaction)async{
                                DocumentSnapshot snapshot=
                                await transaction.get(document);
                                await transaction.delete(snapshot.reference);
                              });
  }  
 

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String data= document[i].data['data'].toString();

            String frigo1am= document[i].data['frigo1am'].toString();
            String frigo1pm= document[i].data['frigo1pm'].toString();

            String frigo2am= document[i].data['frigo2am'].toString();
            String frigo2pm= document[i].data['frigo2pm'].toString();

            String frigo3am= document[i].data['frigo3am'].toString();
            String frigo3pm= document[i].data['frigo3pm'].toString();

            String frigo4am= document[i].data['frigo4am'].toString();
            String frigo4pm= document[i].data['frigo4pm'].toString();

            String frigo5am= document[i].data['frigo5am'].toString();
            String frigo5pm= document[i].data['frigo5pm'].toString();

       
             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: new Text(
                   'Controllo $data'
                ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold ),textAlign: TextAlign.center,
                ),
                subtitle: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                
                
                
               
                ])
                
                
              ),
              new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                              
                              child: Row( children: <Widget>[
                                Icon(Icons.delete,color: Colors.red,),
                                Text('Elimina',style: TextStyle(fontSize: 18),)
                              ], ),
                              
                              onPressed: () {

                                  showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Column(children: <Widget>[Text('Procedere?',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold  )), 
            
            
            
            
            
             ],),
            
            actions: <Widget>[
              
              FlatButton(
                child: Text('Annulla',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  Navigator.pop(context);
                  
                   
                 
                  
                  
                },
              ),
              FlatButton(
                child: Text('Elimina',style: TextStyle(fontSize: 14)),
                onPressed:(){
         
                elimina(document[i].reference);
                Navigator.pop(context);
                
                }
                

                   
                 
                  
                  
                
              )
            ],
          )
        );
 
                              
                              
                              
                              },
                            ),
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.camera),
                        Text('Visualizza',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 

                        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new editfrigo(
                           frigo1Am: frigo1am,
                           frigo1Pm: frigo1pm,

                           frigo2Am: frigo2am,
                           frigo2Pm: frigo2pm,
                           
                           frigo3Am: frigo3am,
                           frigo3Pm: frigo3pm,

                           frigo4Am: frigo4am,
                           frigo4Pm: frigo4pm,

                           frigo5Am: frigo5am,
                           frigo5Pm: frigo5pm,

                           data: data,
                           index: document[i].reference,

                          
                          )
                        )
                        );



                      },
                    ),
                    
                  ],
                ),
              ),
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _tempcronoState createState() => new _tempcronoState();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}

