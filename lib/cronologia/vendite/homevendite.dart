import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/cronologia/prodotti/dettagli.dart';
import 'package:foodapp/cronologia/vendite/prodottivenduti.dart';


class vendite extends StatefulWidget{

  vendite({this.data});
  final String data;
  
  @override
  _venditeState createState() => new _venditeState(); 
}

class _venditeState extends State<vendite>{
 
   String data;

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data=widget.data;
  }
    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Vendite del $data'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("venditafinita").where("dataproduzione", isEqualTo: data)
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatelessWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;

  void elimina(var document){
    Firestore.instance.runTransaction((transaction)async{
                                DocumentSnapshot snapshot=
                                await transaction.get(document);
                                await transaction.delete(snapshot.reference);
                              });
  }

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String nome= document[i].data['cliente'].toString();
            String fattura= document[i].data['fattura'].toString();
          

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: new Text(
                   nome.toUpperCase()
                ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold ),textAlign: TextAlign.center,
                ),
                subtitle: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                   Text('ddt/fattura: $fattura', style: TextStyle(fontSize: 16),  ),
               
                  ]),
                )
                
                
              ),
              new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                     new FlatButton(
                              
                              child: Row( children: <Widget>[
                                Icon(Icons.delete,color: Colors.red,),
                                Text('Elimina',style: TextStyle(fontSize: 18),)
                              ], ),
                              
                              onPressed: () {

                                  showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Column(children: <Widget>[Text('Procedere?',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold  )), 
            
            
            
            
            
             ],),
            
            actions: <Widget>[
              
              FlatButton(
                child: Text('Annulla',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  Navigator.pop(context);
                  
                   
                 
                  
                  
                },
              ),
              FlatButton(
                child: Text('Disabilita',style: TextStyle(fontSize: 14)),
                onPressed:(){
         
                elimina(document[i].reference);
                Navigator.pop(context);
                
                }
                

                   
                 
                  
                  
                
              )
            ],
          )
        );
 
                              
                              
                              
                              },
                            ),
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.add_box),
                        Text('Visualizza acquisti',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 

                        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new elencoprodotti(
                           codicevendita: document[i].data['codicevendita'],
                          
                          )
                        )
                        );


                      },
                    ),
                    
                  ],
                ),
              ),
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _venditeState createState() => new _venditeState();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
