import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/cronologia/prodotti/dettagli.dart';


class elencoprodotti extends StatefulWidget{

  elencoprodotti({this.codicevendita});
  final codicevendita;
  
  @override
  _elencoprodottiState createState() => new _elencoprodottiState(); 
}

class _elencoprodottiState extends State<elencoprodotti>{
 
   var codicevendita;

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    codicevendita=widget.codicevendita;
  }
    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Elenco prodotti'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("uscita").where("codicesessionevendita", isEqualTo: codicevendita)
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatelessWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String nome= document[i].data['nome'].toString();
            String quantita= document[i].data['quantita'].toString();
            String indirizzo= document[i].data['indirizzo'].toString();
            String iva= document[i].data['iva'].toString();
            String lotto= document[i].data['codicelotto'].toString();
            String telefono= document[i].data['telefono'].toString();
            String misura= document[i].data['misura'].toString();

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: new Text(
                   nome.toUpperCase()
                ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold ),textAlign: TextAlign.center,
                ),
                subtitle: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Data/lotto: $lotto',style: new TextStyle(fontSize: 16),),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Quantità: $quantita $misura',style: new TextStyle(fontSize: 16),),
                ),
                
               
                ])
                
                
              ),
              new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.add_box),
                        Text('Visualizza dettagli',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 

                        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new dettagliprodotti(
                           sessione: document[i].data['codiceproduzione'],
                          
                          )
                        )
                        );


                      },
                    ),
                    
                  ],
                ),
              ),
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _elencoprodottiState createState() => new _elencoprodottiState();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
