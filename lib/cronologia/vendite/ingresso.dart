import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:foodapp/track/ingressoa3.dart';
import 'package:flutter/services.dart';
import 'package:foodapp/cronologia/vendite/homevendite.dart';


class ingressovenditacrono extends StatefulWidget{
  ingressovenditacrono({this.nome, this.iva, this.index });
  final String nome;
  final String iva;
  final index;

  @override
  _ingressovenditacronoState createState() => new _ingressovenditacronoState(); 
}

class _ingressovenditacronoState extends State<ingressovenditacrono>{

  String nome;
  String iva;
  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  String _testodata2 = '';
  File image;
  String filename='';
  String indirizzofirebase;
  String pathimg='';
 // String get immagine=> '$iva'+'$_testodata2' ;

  Future _getimage() async{
    var selectedImage= await ImagePicker.pickImage(source: ImageSource.camera,maxHeight: 400,maxWidth: 400);
    setState(() {
          
          image=selectedImage;

        //  filename=immagine;
          filename=basename(image.path);
          pathimg=image.path.toString();

          
        });
  }

  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      nome=widget.nome;
      iva=widget.iva;
      _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);
    }

  @override
    Widget build(BuildContext context) {
        
    void pagina(){
    
    Navigator.of(context).push(new MaterialPageRoute(
                   builder: (BuildContext context)=> new ingressoa3(
                     nome: nome,
                     iva: iva,
                     urlimmagine: indirizzofirebase,
                     pathimmagine:pathimg,
                     data: _testodata,
                     filename: filename,
                    
                     
                     
                     
                   )
      )
    );}
   return new Scaffold(

    
    appBar: 
      new AppBar(
        title: Text('Filtro'),
      ),

      
    
    
    body: 
       Column(
       children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: new Column(
                children: <Widget>[
                  new ListTile(
                    title: new Row(
                      children: <Widget>[
                        new Expanded( child: Text("Data vendita:", style:new TextStyle(fontSize:22.0,color: Colors.black ))),
                         new FlatButton(
                          onPressed: ()=> _selezionadata(context),
                           child:Text(_testodata, style:new TextStyle(fontSize:22.0,color: Colors.black )))
                      ],
                    ),
                  )
                ],
              ),

            ),
          ),

         Padding(
           padding: const EdgeInsets.all(8),
           child: Card(
             child: Column(
               children: <Widget>[
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Text('Dopo aver selzionato la data clicca su visualizza'),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: RaisedButton(
                     onPressed: (){
                       Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new vendite(
                        data: _testodata,
                      )),
                    );


                     },
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                         Text('Visualizza')
                       ],
                     ),
                   ),
                 )
               ],
             ),
           ),
         )
         
         
         
          
 
        
       ],
       


     ),

     

    
    
    
    );


    
    
    
    
    }

    




}

