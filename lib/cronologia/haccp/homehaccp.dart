import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/cronologia/prodotti/dettagli.dart';
import 'package:foodapp/cronologia/vendite/prodottivenduti.dart';


class haccp extends StatefulWidget{

  haccp({this.data});
  final String data;
  
  @override
  _haccpState createState() => new _haccpState(); 
}

class _haccpState extends State<haccp>{
 
   String data;

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data=widget.data;
  }
    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Haccp del $data'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("controllogiornaliero").where("data", isEqualTo: data)
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatelessWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String nome= document[i].data['macchinario'].toString();
            String immagine= document[i].data['pathimmagine'].toString();
            String conforme= document[i].data['conformità'].toString();
          

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: new Text(
                   nome.toUpperCase()
                ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold ),textAlign: TextAlign.center,
                ),
                subtitle: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.network(immagine,width: 150,height: 150),
                ),
                 Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Conformità rilevata: $conforme', style: new TextStyle(fontSize: 18),)
                ),

               
                ])
                
                
              ),
              
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _haccpState createState() => new _haccpState();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
