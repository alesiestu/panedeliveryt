import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:foodapp/track/ingressoa3.dart';
import 'package:flutter/services.dart';
import 'package:foodapp/cronologia/vendite/homevendite.dart';
import 'package:foodapp/haccp/temperaturefrigo2.dart';
import 'package:foodapp/haccp/newfrigo.dart';
import 'package:foodapp/haccp/home.dart';



class editfrigo extends StatefulWidget{
  editfrigo({this.data, this.frigo1Am, this.frigo1Pm,this.frigo2Am, this.frigo2Pm, this.frigo3Am, this.frigo3Pm,this.frigo4Am, this.frigo4Pm,this.frigo5Am, this.frigo5Pm, this.index });
  
  final index;
  
  final String frigo1Am;
  final String frigo1Pm;

  final String frigo2Am;
  final String frigo2Pm;

  final String frigo3Am;
  final String frigo3Pm;

  final String frigo4Am;
  final String frigo4Pm;

  final String frigo5Am;
  final String frigo5Pm;

  final data;

  @override
  _editfrigoState createState() => new _editfrigoState(); 
}

class _editfrigoState extends State<editfrigo>{

 
  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  String _testodata2 = '';
  File image;
  String filename='';
  String indirizzofirebase;
  String pathimg='';
  
  String frigo1Am;
  String frigo1Pm;

  String frigo2Am;
  String frigo2Pm;

  String frigo3Am;
  String frigo3Pm;

  String frigo4Am;
  String frigo4Pm;

  String frigo5Am;
  String frigo5Pm;


  TextEditingController controllerfrigo1am;
  TextEditingController controllerfrigo1pm;

  TextEditingController controllerfrigo2am;
  TextEditingController controllerfrigo2pm;

  TextEditingController controllerfrigo3am;
  TextEditingController controllerfrigo3pm;

  TextEditingController controllerfrigo4am;
  TextEditingController controllerfrigo4pm;

  TextEditingController controllerfrigo5am;
  TextEditingController controllerfrigo5pm;

  var data;


 // String get immagine=> '$iva'+'$_testodata2' ;

  

  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }


  conferma(context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Inserimento effettuato"),
            ],
          ),
         
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new FlatButton(
                  child: new Text("OK"),
                  onPressed: () {
                    

                                 Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new homehaccp(),
                        ),
                        ModalRoute.withName('/'));
                  },
                ),
              ],
            ),
           
          ],
        );
      },
    );
}

  void _addData(){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('temp');
       await reference.add({
         "data":_testodata,

         "frigo1am":frigo1Am,
         "frigo1pm":frigo1Pm,

         "frigo2am":frigo2Am,
         "frigo2pm":frigo2Pm,

         "frigo3am":frigo3Am,
         "frigo3pm":frigo3Pm,

         "frigo4am":frigo4Am,
         "frigo4pm":frigo4Pm,

         "frigo5am":frigo5Am,
         "frigo5pm":frigo5Pm,

       });
     });
    
  }


    void updatedata(){
  
    Firestore.instance.runTransaction((Transaction transaction)async {
      DocumentSnapshot snapshot=
      await transaction.get(index);
      await transaction.update(snapshot.reference, {
        "frigo1am":frigo1Am,
         "frigo1pm":frigo1Pm,

         "frigo2am":frigo2Am,
         "frigo2pm":frigo2Pm,

         "frigo3am":frigo3Am,
         "frigo3pm":frigo3Pm,

         "frigo4am":frigo4Am,
         "frigo4pm":frigo4Pm,

         "frigo5am":frigo5Am,
         "frigo5pm":frigo5Pm,

      });
    });
  

  }


String test;
var index;

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
     
      _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp, ]);

      frigo1Am=widget.frigo1Am;
      frigo1Pm=widget.frigo1Pm;

      frigo2Am=widget.frigo2Am;
      frigo2Pm=widget.frigo2Pm;

      frigo3Am=widget.frigo3Am;
      frigo3Pm=widget.frigo3Pm;

      frigo4Am=widget.frigo4Am;
      frigo4Pm=widget.frigo4Pm;

      frigo5Am=widget.frigo5Am;
      frigo5Pm=widget.frigo5Pm;

     

      controllerfrigo1am=new TextEditingController(text: widget.frigo1Am);
      controllerfrigo1pm=new TextEditingController(text: widget.frigo1Pm);

      controllerfrigo2am=new TextEditingController(text: widget.frigo2Am);
      controllerfrigo2pm=new TextEditingController(text: widget.frigo2Pm);

      controllerfrigo3am=new TextEditingController(text: widget.frigo3Am);
      controllerfrigo3pm=new TextEditingController(text: widget.frigo3Pm);

      controllerfrigo4am=new TextEditingController(text: widget.frigo4Am);
      controllerfrigo4pm=new TextEditingController(text: widget.frigo4Pm);;

      controllerfrigo5am=new TextEditingController(text: widget.frigo5Am);
      controllerfrigo5pm=new TextEditingController(text: widget.frigo5Pm);
      
     data=widget.data;
     index=widget.index;
    }

  @override
    Widget build(BuildContext context) {
        
    
   return new Scaffold(

     resizeToAvoidBottomPadding : false,
    appBar: 
      new AppBar(
        title: Text('Controllo del $data'),
      ),

      floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.save),
       backgroundColor: Colors.blue,
       onPressed: (){
         updatedata();
         conferma(context);


       }
              
     ),
     floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
     bottomNavigationBar: new BottomAppBar(
       elevation: 20,
       color: Colors.grey,
       child: ButtonBar(
         children: <Widget>[],
       ),
     ),

      
    
    body: 
       Column(
       children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: new Column(
                children: <Widget>[
                  new ListTile(
                    title: new Row(
                      children: <Widget>[
                        new Expanded( child: Text("Data controllo:", style:new TextStyle(fontSize:22.0,color: Colors.black ))),
                         new FlatButton(
                          onPressed: ()=> _selezionadata(context),
                           child:Text(_testodata, style:new TextStyle(fontSize:22.0,color: Colors.black )))
                      ],
                    ),
                  )
                ],
              ),

            ),
          ),

         Padding(
           padding: const EdgeInsets.all(2),
           child: Card(
             child: Padding(
               padding: const EdgeInsets.all(2.0),
               child: 
               
               Column(
                 children: <Widget>[
                   new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Text('Frigo  '.toUpperCase(), style: TextStyle(fontSize:18.0,color: Colors.black ),)
                      ),
                    ),
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Text('AM C°        ')
                        ),
                      ),
                    new Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Text('PM C°        ')
                      ),
                    ),
              ],
            ),


                   new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Text('Frigo 1'.toUpperCase(), style: TextStyle(fontSize:18.0,color: Colors.black ),)
                      ),
                    ),
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: new TextField(
                             controller: controllerfrigo1am,
                              onChanged: (String str){
                              setState(() {
                              frigo1Am=str;        
                                              });
                            },
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(5)
                              )
                          ),
                        ),
                      ),
                    new Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: new TextField(
                          controller: controllerfrigo1pm,
                          onChanged: (String str){
                              setState(() {
                              frigo1Pm=str;        
                                              });
                            },
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(5)
                            )
                        ),
                      ),
                    ),
              ],
            ),
           new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Text('Frigo 2'.toUpperCase(), style: TextStyle(fontSize:18.0,color: Colors.black ),)
                      ),
                    ),
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: new TextField(
                             controller: controllerfrigo2am,
                            onChanged: (String str){
                              setState(() {
                              frigo2Am=str;        
                                              });
                            },
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(5)
                              )
                          ),
                        ),
                      ),
                    new Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: new TextField(
                          controller: controllerfrigo2pm,
                          onChanged: (String str){
                              setState(() {
                              frigo2Pm=str;        
                                              });
                            },
                          keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(5)
                            )
                        ),
                      ),
                    ),
              ],
            ),
            new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Text('Frigo 3'.toUpperCase(), style: TextStyle(fontSize:18.0,color: Colors.black ),)
                      ),
                    ),
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: new TextField(
                            controller: controllerfrigo3am,
                            onChanged: (String str){
                              setState(() {
                              frigo3Am=str;        
                                              });
                            },
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(5)
                              )
                          ),
                        ),
                      ),
                    new Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: new TextField(
                          controller: controllerfrigo3pm,
                          onChanged: (String str){
                              setState(() {
                              frigo3Pm=str;        
                                              });
                            },
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(5)
                            )
                        ),
                      ),
                    ),
              ],
            ),
          new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Text('Frigo 4'.toUpperCase(), style: TextStyle(fontSize:18.0,color: Colors.black ),)
                      ),
                    ),
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: new TextField(
                            controller: controllerfrigo4am,
                            onChanged: (String str){
                              setState(() {
                              frigo4Am=str;        
                                              });
                            },
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(5)
                              )
                          ),
                        ),
                      ),
                    new Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: new TextField(
                          controller: controllerfrigo4pm,
                          onChanged: (String str){
                              setState(() {
                              frigo4Pm=str;        
                                              });
                            },
                          keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(5)
                            )
                        ),
                      ),
                    ),
              ],
            ),
            new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Text('Frigo 5'.toUpperCase(), style: TextStyle(fontSize:18.0,color: Colors.black ),)
                      ),
                    ),
                      new Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: new TextField(
                            controller: controllerfrigo5am,
                            onChanged: (String str){
                              setState(() {
                              frigo5Am=str;        
                                              });
                            },
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(5)
                              )
                          ),
                        ),
                      ),
                    new Flexible(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: new TextField(
                          controller: controllerfrigo5pm,
                          onChanged: (String str){
                              setState(() {
                              frigo5Pm=str;        
                                              });
                            },
                          keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(5)
                            )
                        ),
                      ),
                    ),
              ],
            ),

                 
                 
                 
                 
                 
                 ],
               ),



               
             ),
           ),
         )
         
         
         
          
 
        
       ],
       


     ),

     

    
    
    
    );


    
    
    
    
    }

    



Future<String> uploadfirebase() async{
  StorageReference ref= FirebaseStorage.instance.ref().child(filename);
  StorageUploadTask uploadTask=ref.putFile(image);
  var downurl = await (await uploadTask.onComplete).ref.getDownloadURL();
  var url=downurl.toString();
  print(url);
  
 
  
  setState(() {
        url=indirizzofirebase;
        
     
          });
  
  return url;
}
}

