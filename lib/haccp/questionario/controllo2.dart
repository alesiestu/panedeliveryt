import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/ingressoa2.dart';
import 'package:foodapp/haccp/newcontrollo.dart';
import 'package:foodapp/haccp/questionario/controllo3.dart';

class controllo2 extends StatefulWidget{
  controllo2({this.data,this.sessione});
  final String data;
  final int sessione;
  
  @override
  _controllo2State createState() => new _controllo2State(); 
}

class _controllo2State extends State<controllo2>{

  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  
  String data;
  int sessione;
  

  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
     _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
     sessione=widget.sessione;
     data=widget.data;
  }

  
  
 

    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Controllo giornaliero'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("controllore").where("controllo", isEqualTo: "Controllo2")
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,data: data,sessione: sessione,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatelessWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path,this.data,this.sessione});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;

  final data;
  final int sessione;
  void _addData(String domanda, String pathimmagine, int sessione, String nome){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('controllogiornaliero');
       await reference.add({
         "macchinario":domanda,
         "pathimmagine":pathimmagine,
         "data": data,
         "conformità":"si",
         "nome": nome,
         "codicesessione":sessione

       });
     });
     
  }

  void _addDatand(String domanda, String pathimmagine, int sessione, String nome){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('controllogiornaliero');
       await reference.add({
         "macchinario":domanda,
         "pathimmagine":pathimmagine,
         "data": data,
         "conformità":"no",
         "nome": nome,
         "codicesessione":sessione

       });
     });
     
  }

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String nome= document[i].data['nome'].toString();
            String domanda= document[i].data['domanda'].toString();
            String pathimmagine= document[i].data['urlfirebase'].toString();
            

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: Column(
                  children: <Widget>[
                    new Text(
                       nome.toUpperCase()
                    ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold ),
                    ),
                  ],
                ),
                subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                  //    Image.network(pathimmagine,width: 300,height: 300),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(domanda,style: TextStyle(fontSize: 20),),
                      ),
                    ],
                  ),
                ),
               
                ])
                
                
              ),
              new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.add_alert,color: Colors.red,),
                        Text('Non conforme',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 
                         _addDatand(domanda, pathimmagine, sessione, nome) ;
                        Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new controllo3(
                        
                        data: data,
                        sessione: sessione,

                      )
                      
                      ),
                    );
                       
                      },
                    ),
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.add_box),
                        Text('Conforme',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 
                      _addData(domanda, pathimmagine, sessione, nome) ;
                      Navigator.push(
                      context,
                      MaterialPageRoute(
                      builder: (BuildContext context) => new controllo3(
                        
                        data: data,
                        sessione: sessione,

                      )
                      
                      ),
                    );
                      
                      },
                    )
                      
                    
                  ],
                ),
              ),
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _controllo2State createState() => new _controllo2State();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
