import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:foodapp/haccp/home.dart';
import 'package:flutter/services.dart';


class riepilogo extends StatefulWidget{
  riepilogo({this.controllo,this.filename,this.pathimmagine,this.urlimmagine,this.idmacchina });
  final String urlimmagine;
  final String pathimmagine;
  final String filename;
 
  final String controllo;
  final idmacchina;

  @override
  _controlloState createState() => new _controlloState(); 
}


class _controlloState extends State<riepilogo>{

  String nome;
  String iva;
  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  String _testodata2 = '';
  File image;
  String filename='';
  String appoggio;
  String indirizzofirebase;
  String pathimg='';

  String controllo='';
  var idmacchina;
  String domanda='';

 // String get immagine=> '$iva'+'$_testodata2' ;



  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
     // pathimg=widget.pathimmagine;
      //indirizzofirebase=widget.urlimmagine;
      controllo=widget.controllo;
      idmacchina=widget.idmacchina;
      //filename=widget.filename;
      //printUrl();
     
      _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);
    }

    
  void updateclienti(){
  
    Firestore.instance.runTransaction((Transaction transaction)async {
      DocumentSnapshot snapshot=
      await transaction.get(idmacchina);
      await transaction.update(snapshot.reference, {
       // "controllo": controllo,
        "urlimmagine":"default",
        "domanda":domanda,
        

      });
    });
   

  }



    
   void addcheck(){
      
       Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('controllore');
       await reference.add({
        "nome":controllo,
        "urldisposivito":pathimg,
        "urlfirebase":appoggio
        
       });
     });
  }

   printUrl() async {
    StorageReference ref = 
    FirebaseStorage.instance.ref().child(filename);
    String urlimmagine = (await ref.getDownloadURL()).toString();
     setState(() {
        appoggio=urlimmagine;
        
     
          });
    print(urlimmagine);
    
    
     }

  

  @override
    Widget build(BuildContext context) {
        
  
   
   return new Scaffold(

    
    appBar: 
      new AppBar(
        title: Text('$controllo'),
      actions: <Widget>[
        
      ],
      
      
      
      ),



    
    
    
    body:
       
        
       Card(
         child: Column(
           children: <Widget>[
              ListTile(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                     
                      Padding(
                        padding: const EdgeInsets.only(top:20.0),
                        child: Text('Questionario senza immagine', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                      ),
                  ],
                ),
                subtitle: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                   // Image.file(File(pathimg),width: 200, height: 300,),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text('Testo da visualizzare: ' ,style: TextStyle(fontSize: 18),),
                    ),

                    //Text(controllo, style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TextField(
                                          onChanged: (String str){
                                            setState(() {
                                            domanda=str;        
                                                            });
                                          },
                                          decoration: new InputDecoration(
                                          icon: Icon(Icons.add_to_queue),
                                          hintText: "Scrivere qui",
                                            

                                          ),
                                        ),
                                      ),
                     
                     Padding(
                      padding: const EdgeInsets.only(top:18.0),
                      child: 
                      RaisedButton(
                        onPressed: (){
                           updateclienti(); 
                           showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Column(children: <Widget>[Text('Controllo salvato'), 
            
             ],),
            
            actions: <Widget>[
             
              FlatButton(
                child: Text('Torna alla home',style: TextStyle(fontSize: 14)),
                onPressed:(){
                 

                Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new homehaccp(),
                        ),
                        ModalRoute.withName('/'));

                
                }
              )
            ],
          )
        );

                          },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('SALVA')
                          ],
                        ),
                      )
                    ),
                  ],
                ),
              )
              
             
         
            
             
           ],
         ),
       )
    
       
    
       


     

     

    
    
    
    );


    
    
    
    
    }

    




}

