import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:foodapp/track/ingressoa3.dart';
import 'package:flutter/services.dart';
import 'package:foodapp/haccp/riepilogo.dart';
import 'package:foodapp/haccp/riepilogoconfoto.dart';



class newcontrollo extends StatefulWidget{
  
  newcontrollo({this.id,this.urlimmagine,this.nomemacchinario});
  final String urlimmagine;
  final String nomemacchinario;
  final id;
  

  @override
  _controlloState createState() => new _controlloState(); 
}


class _controlloState extends State<newcontrollo>{

  String nome;
  String iva;
  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  String _testodata2 = '';
  File image;
  String filename='';
  String indirizzofirebase;
  String pathimg='';

  String immaginemacchine;
  var idmacchina;
  String nomemacchina;

  String controllo='';
 // String get immagine=> '$iva'+'$_testodata2' ;

  Future _getimage() async{
    var selectedImage= await ImagePicker.pickImage(source: ImageSource.camera,maxHeight: 400,maxWidth: 400);
    setState(() {
          
          image=selectedImage;

        //  filename=immagine;
          filename=basename(image.path);
          pathimg=image.path.toString();



          

          
        });
  }

 

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      nomemacchina=widget.nomemacchinario;
      idmacchina=widget.id;
      immaginemacchine=widget.urlimmagine;
      
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
      

  ]);
    }


    


  @override
    Widget build(BuildContext context) {
        
    void pagina(){
    
    Navigator.of(context).push(new MaterialPageRoute(
                   builder: (BuildContext context)=> new riepilogo(
                     
                     urlimmagine: indirizzofirebase,
                     pathimmagine:pathimg,
                     filename: filename,
                     controllo: controllo,
                    
                     
                     
                     
                   )
      )
    );}


      void paginafoto(){
    
    Navigator.of(context).push(new MaterialPageRoute(
                   builder: (BuildContext context)=> new riepilogoconfoto(
                     
                     urlimmagine: indirizzofirebase,
                     pathimmagine:pathimg,
                     filename: filename,
                     controllo: controllo,
                     idmacchina: idmacchina,
                     
                     
                     
                   )
      )
    );}
   return new Scaffold(

    
    appBar: 
      new AppBar(
        title: Text('$nomemacchina'),
     
      
      
      
      ),



//      floatingActionButton: new FloatingActionButton(
      // child: Icon(Icons.photo_camera),
      // backgroundColor: Colors.blue,
      // onPressed: _getimage,     
    // ),
    
    
     floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
     bottomNavigationBar: new BottomAppBar(
       elevation: 20,
       color: Colors.grey,
       child: ButtonBar(
         children: <Widget>[],
       ),
     ),
    
    
    body: 
       Column(
       children: <Widget>[
          

          Padding(
            padding: const EdgeInsets.all(16.0),
            child: 
            new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              
              
            
            
            
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  image==null?Column(children: <Widget>[
                    Text("Modifica controllo 1", style: new TextStyle(fontSize: 20,fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
                    
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        onPressed: (){
                            Navigator.push(
                            context,
                            MaterialPageRoute(
                            builder: (BuildContext context) => new riepilogo(
                           urlimmagine: indirizzofirebase,
                            pathimmagine:pathimg,
                            filename: filename,
                            controllo: controllo,
                            idmacchina: idmacchina,

                      )
                      
                      ),
                    );
                        },
                        child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('Procedi',style: TextStyle(fontSize: 20),)

                        ],
                      ),),
                    )
                    
                    ]):(

                     
                     Column(children: <Widget>[
                     
                     
                     Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                       children: <Widget>[
                         
                         new ListTile(
                          
                          title: Column(
                            children: <Widget>[ 
                              Card(
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Image.file(image, width: 300, height: 300,),
                                    ),
                                     Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        onPressed: (){
                            uploadfirebase().whenComplete(paginafoto);
                        },
                        child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('Compila dati',style: TextStyle(fontSize: 20),)

                        ],
                      ),),
                    )
                                  ],
                                ),
                                
                              ),
                               Padding(
                                  padding: const EdgeInsets.only(top:12.0),
                                  child: Column(
                                    children: <Widget>[
                                     
                                     
                                    ],
                                  ),
                                ),
                              
                                          ],
                                        )
                                        
                                        
                                      ),
                          
                          
                         
                                        ],
                                      ),
                                      
                                      
                                      
                                        

                                      ],)

                                      ),
                ],
              ),
            ),
            
                              
            


            ],
            ),
          ),
         
         
         
          
 
        
       ],
       


     ),

     

    
    
    
    );


    
    
    
    
    }

  String _bytesTransferred(StorageTaskSnapshot snapshot) {
    double res = snapshot.bytesTransferred / 1024.0;
    double res2 = snapshot.totalByteCount / 1024.0;
    return '${res.truncate().toString()}/${res2.truncate().toString()}';
  }

Widget _uploadStatus(StorageUploadTask task) {
    return StreamBuilder(
      stream: task.events,
      builder: (BuildContext context, snapshot) {
        Widget subtitle;
        if (snapshot.hasData) {
          final StorageTaskEvent event = snapshot.data;
          final StorageTaskSnapshot snap = event.snapshot;
          subtitle = Text('${_bytesTransferred(snap)} KB sent');
        } else {
          subtitle = const Text('Starting...');
        }
        return ListTile(
          title: task.isComplete && task.isSuccessful
              ? Text(
                  'Done'
                )
              : Text(
                  'Uploading'
                ),
          subtitle: subtitle,
        );
      },
    );
  }

    



Future<String> uploadfirebase() async{
  StorageReference ref= FirebaseStorage.instance.ref().child(filename);
  StorageUploadTask uploadTask=ref.putFile(image);
  _uploadStatus(uploadTask);
  var downurl = await (await uploadTask.onComplete).ref.getDownloadURL();
  var urlimm=downurl.toString();

  



  print(urlimm);
  setState(() {
        urlimm=indirizzofirebase;
          });
  
  return urlimm;
}
}

