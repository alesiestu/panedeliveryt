import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/ingressoa2.dart';
import 'package:foodapp/haccp/newcontrollo.dart';


class controllo extends StatefulWidget{
  
  @override
  _controlloState createState() => new _controlloState(); 
}

class _controlloState extends State<controllo>{
 

    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Modifica questionario'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("controllore")
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatelessWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String nome= document[i].data['nome'].toString();
            String domanda= document[i].data['domanda'].toString();
            String pathimmagine= document[i].data['urlfirebase'].toString();
            

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: Column(
                  children: <Widget>[
                    new Text(
                       nome.toUpperCase()
                    ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold ),
                    ),
                  ],
                ),
                subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                    //  Image.network(pathimmagine,width: 300,height: 300),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Nome visualizzato:', style: TextStyle(fontSize: 16),),
                            ),
                            Text(domanda.toUpperCase(), style: TextStyle(fontSize: 18)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
               
                ])
                
                
              ),
              new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.add_box),
                        Text('Modifica controllo',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 
                        Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new newcontrollo(
                        urlimmagine: pathimmagine,
                        nomemacchinario: nome,
                        id: document[i].reference,

                      )
                      
                      ),
                    );
                      },
                    )
                      
                    
                  ],
                ),
              ),
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _controlloState createState() => new _controlloState();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
