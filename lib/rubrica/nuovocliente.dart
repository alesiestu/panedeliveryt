import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class Nuovocliente extends StatefulWidget {
  
  
  @override
  _NuovoclienteState createState() => new _NuovoclienteState();
}

class _NuovoclienteState extends State<Nuovocliente> {

  
  
  String nome='';
  String indirizzo='';
  String telefono='';
  String iva='';




  

  
  

  void _addData(){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('clienti');
       await reference.add({
         "nome" : nome,
         "indirizzo": indirizzo,
         "telefono":telefono,
         "iva":iva

       });
     });
     Navigator.pop(context);
  }


  
 

  @override
  Widget build(BuildContext context) {
    return new Material(
      child: Column(
        children: <Widget>[
          Container(
            height: 200.0,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.blue
            ),
          child: Column( 
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            Text("Nuovo cliente",style: new TextStyle(
              color: Colors.white,
              fontSize: 30.0,
              letterSpacing: 2.0,
              fontFamily: "Pacifico"
            ),)
          ],
          ),
          ),
          new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              onChanged: (String str){
                setState(() {
                 nome=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.dashboard),
                hintText: "Nome o ragione sociale",
                border: InputBorder.none

              ),
            ),
          ),
         
          new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              onChanged: (String str){
                setState(() {
                 indirizzo=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.note),
                hintText: "Indirizzo",
                border: InputBorder.none

              ),
            ),
          ),
          new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              keyboardType: TextInputType.number,
              onChanged: (String str){
                setState(() {
                 telefono=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.note),
                hintText: "telefono",
                border: InputBorder.none

              ),
            ),
          ),
           new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              onChanged: (String str){
                setState(() {
                 iva=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.note),
                hintText: "Partita iva",
                border: InputBorder.none

              ),
            ),
          ),

          new Padding(
            padding: const EdgeInsets.only(top:100),
            child:
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.check, size: 40.0,),
                onPressed: (){
                  _addData();
                } ,
              ),
              IconButton(
                icon: Icon(Icons.close, size: 40.0,),
                onPressed: (){
                  Navigator.pop(context);
                } ,
              )
            ],
          )
          ),











        ],
      ),
      
    );
  }



  
   



  
}