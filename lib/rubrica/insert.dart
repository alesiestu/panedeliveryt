import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class InsertPage extends StatefulWidget {
  InsertPage({this.email});
  final String email;
  @override
  _InsertPageState createState() => new _InsertPageState();
}

class _InsertPageState extends State<InsertPage> {

  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  
  String prodotto='';
  String note='';



  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
          });
   }
  }

  void _addData(){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('clienti');
       await reference.add({
         "email" : widget.email,
         "prodotto": prodotto,
         "note":note,
         "data":_testodata

       });
     });
     Navigator.pop(context);
  }


  
   @override
     void initState() {
       // TODO: implement initState
       super.initState();
        _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
     }

  @override
  Widget build(BuildContext context) {
    return new Material(
      child: Column(
        children: <Widget>[
          Container(
            height: 200.0,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.blue
            ),
          child: Column( 
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            Text("Aggiungi prodotto",style: new TextStyle(
              color: Colors.white,
              fontSize: 30.0,
              letterSpacing: 2.0,
              fontFamily: "Pacifico"
            ),)
          ],
          ),
          ),
          new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              onChanged: (String str){
                setState(() {
                 prodotto=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.dashboard),
                hintText: "Nome",
                border: InputBorder.none

              ),
            ),
          ),
          new Padding(
          padding: const EdgeInsets.all(16.0),
          child:
          new Row(
          children: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(right:16.0),
            child:   
          new Icon(Icons.date_range),
          ),
          new Expanded( child: Text("Seleziona data", style:new TextStyle(fontSize:22.0,color: Colors.black ))),
          new FlatButton(
            onPressed: ()=> _selezionadata(context),
            child:Text(_testodata, style:new TextStyle(fontSize:22.0,color: Colors.black )))
          ],
          ),
          ),
          new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              onChanged: (String str){
                setState(() {
                 note=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.note),
                hintText: "Note",
                border: InputBorder.none

              ),
            ),
          ),

          new Padding(
            padding: const EdgeInsets.only(top:100),
            child:
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.check, size: 40.0,),
                onPressed: (){
                  _addData();
                } ,
              ),
              IconButton(
                icon: Icon(Icons.close, size: 40.0,),
                onPressed: (){
                  Navigator.pop(context);
                } ,
              )
            ],
          )
          ),











        ],
      ),
      
    );
  }



  
   



  
}