import 'package:flutter/material.dart';
import 'package:foodapp/auth.dart';
import 'package:foodapp/rubrica/client.dart';
import 'package:foodapp/track/stadi.dart';
import 'package:foodapp/rubrica/fornitori.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:foodapp/haccp/home.dart';
import 'package:foodapp/cronologia/home.dart';
import 'package:foodapp/magazzino/home.dart';
import 'package:foodapp/test2.dart';
import 'package:foodapp/magazzino/homestore.dart';
import 'package:foodapp/address/home.dart';


class HomePage extends StatelessWidget {
  var email;

  HomePage({this.auth, this.onSignOut, Key id, this.email});
  final BaseAuth auth;
  final VoidCallback onSignOut;
  FirebaseUser currentUser; 

  

  String _email() {
    if (currentUser != null) {
      return currentUser.displayName;
    } else {
      return "no current user";
    }
  }

  Future<String> currentUser1() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    return user.email;
  }

Future<String> inputData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String uid = user.uid.toString();
  return uid;
}
 
 
 void printemail() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String uid = user.uid.toString();
    final String email = user.email.toString();
  print(email);
}
  


  @override
  Widget build(BuildContext context) {

    void _signOut() async {
      try {
        await auth.signOut();
        onSignOut();
      } catch (e) {
        print(e);
      }

    }
    



    return new Scaffold(
      appBar: new AppBar(
        title: Text('NomeAPP'),
    
        actions: <Widget>[

          new RaisedButton(
             color: Colors.blue,
             
            // onPressed: _signOut,
            onPressed: 
              _signOut,
            
             //child: new Text('Logout', style: new TextStyle(fontSize: 17.0, color: Colors.white)),
             child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  
                  Padding(
                    padding: const EdgeInsets.only(right:8.0),
                    child: new Text('Logout',style: new TextStyle(fontSize: 17.0, color: Colors.white)),
                  ),
                  new Icon(Icons.exit_to_app),
                ],
              ),
            
          )
        ],
      ),
      
      body: StaggeredGridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 12.0,
                mainAxisSpacing: 12.0,
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                children: <Widget>[
                  _buildTile(
                   
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                          (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              
                              Text('Ordina ora', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                            ],
                          ),
                          Material
                          (
                            
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(24.0),
                            child: Center
                            (
                              child: Padding
                              (
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.timeline, color: Colors.white, size: 30.0),
                                
                              )
                            )
                          )
                        ]
                      ),
                    ),
                    onTap: (){
                    
                    Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new stadi()),
                    );
                    
                    }
                  ),
                   _buildTile(
                   
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                          (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              
                              Text('Stato ordine', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                            ],
                          ),
                          Material
                          (
                            
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(24.0),
                            child: Center
                            (
                              child: Padding
                              (
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.timeline, color: Colors.white, size: 30.0),
                                
                              )
                            )
                          )
                        ]
                      ),
                    ),
                    onTap: (){
                      Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new homehaccp()),
                    );}
                    
                  ),
                  
                  
                  _buildTile(
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Column
                      (
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Material
                          (
                            color: Colors.red,
                            shape: CircleBorder(),
                            child: Padding
                            (
                              padding: EdgeInsets.all(16.0),
                              child: Icon(Icons.verified_user, color: Colors.white, size: 30.0),
                            )
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 16.0)),
                          Text('Servizio clienti', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 18.0)),
                          
                        ]
                      ),
                    ),
                    onTap: (){
                     Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new homecrono()),
                    );
                    }
                  ),
                  _buildTile(
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Column
                      (
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Material
                          (
                            color: Colors.brown,
                            shape: CircleBorder(),
                            child: Padding
                            (
                              padding: EdgeInsets.all(16.0),
                              child: Icon(Icons.home, color: Colors.white, size: 30.0),
                            )
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 16.0)),
                          Text('Indirizzo', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 18.0)),
                          
                        ]
                      ),
                    ),
                    onTap: (){
                     Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new homeindirizzo()),
                    );
                    }
                  ),
                  
                 
                ],
                staggeredTiles: [
                  StaggeredTile.extent(2, 110.0),
                  StaggeredTile.extent(2, 110.0),
                  StaggeredTile.extent(1, 180.0),
                  StaggeredTile.extent(1, 180.0),
                  StaggeredTile.extent(1, 180.0),
                  StaggeredTile.extent(1, 180.0),
              
                  
                ],
              )
            );
    
  }
   Widget _buildTile(Widget child, {Function() onTap}) {
            return Material(
              elevation: 14.0,
              borderRadius: BorderRadius.circular(12.0),
              shadowColor: Color(0x802196F3),
              child: InkWell
              (
                // Do onTap() if it isn't null, otherwise do print()
                onTap: onTap != null ? () => onTap() : () { print('Not set yet'); },
                child: child
              )
            );
          }

}